from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from shop.models import Category, Product, UserProfile


class ProductDetailAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}

class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False

class UserAdmin(UserAdmin):
    inlines = (UserProfileInline, )

admin.site.register(Product, ProductDetailAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)