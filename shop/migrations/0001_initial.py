# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=79)),
                ('description', models.CharField(max_length=1024)),
                ('price', models.IntegerField()),
                ('photo', models.ImageField(upload_to=b'media')),
            ],
        ),
    ]
