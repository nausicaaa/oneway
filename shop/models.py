from django.db import models
from django.db.models import ImageField
from django.db.models.fields import CharField, IntegerField, SlugField
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User

class Category(models.Model):
    name = CharField(max_length=79)
    slug = SlugField()

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name


class Product(models.Model):
    name = CharField(max_length=79)
    description = CharField(max_length=1024)
    price = IntegerField()
    photo = ImageField()
    slug = SlugField()
    category = models.ForeignKey(Category, null=True)

    def __unicode__(self):
        return self.name


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    username = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    street = models.CharField(max_length=100)
    city_code = models.CharField(max_length=100)

    def __unicode__(self):
        return self.username

class Cart(models.Model):
    creation_date = models.DateTimeField()
    checked_out = models.BooleanField(default=False)
    owner = models.ForeignKey(User)
    item = models.ForeignKey(Product)

    def __unicode__(self):
        return unicode(self.creation_date)
