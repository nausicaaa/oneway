from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from forms import AddItemToCartForm

from shop.models import Category, Product, Cart


class HomeTemplateView(TemplateView):
    template_name = 'shop/index.html'

    def get_context_data(self, **kwargs):
        context = super(HomeTemplateView, self).get_context_data(**kwargs)
        context['products'] = Product.objects.all()[:10]

        return context


class ProductListView(ListView):
    model = Product


class ProductDetailView(DetailView):
    model = Product

    def product_detail(self, request, slug):
        product_detail_slug = Product.objects.get(slug=slug)
        return render(request, 'product_detail.html', {'product': product_detail_slug})


class CategoryListView(ListView):
    model = Category


class CategoryDetailView(DetailView):
    model = Category


class CartDetailView(ListView):
    model = Product

    def add_items(request):
            form = AddItemToCartForm(request)
            cart = Cart.get_or_create(owner=form['user_pk'])
            items = cart.product_set.add(form['product_pk'])
            for item in items:
                item.save()
            #return super(AddItemToCartForm).save()
            return render(request, 'shop/add_items.html', {'form': form})
