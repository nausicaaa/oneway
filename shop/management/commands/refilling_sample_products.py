import os
import sys
from random import randint

from django.core.files import File
from django.core.management.base import BaseCommand
from django.utils.text import slugify
from faker import Faker

from shop.models import Category, Product


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('-x', '--count', type=int, required=True)

    def handle(self, *args, **options):
        filename = 'sample.png'
        relative_path = os.path.realpath(__file__)
        filepath = os.path.join(os.path.dirname(relative_path), filename)
        with open(filepath) as f:
            thefile = File(f)
            # Product.photo.save('sample.png', thefile, True)
            count = options['count']
            faker = Faker()
            Product.objects.all().delete()
            Category.objects.all().delete()
            category = Category.objects.create(name='general', slug='general')

            for i in range(count):
                name = faker.username()
                slug = slugify(name)
                Product.objects.create(
                    name=name,
                    description=faker.lorem(),
                    price=randint(100, 200),
                    slug=slug,
                    category=category,
                    photo=thefile
                )
                sys.stdout.write('.')
                sys.stdout.flush()
        print
