from __future__ import with_statement
from fabric.api import run, cd, env

env.use_ssh_config = True
env.hosts = ['aneta']

def deploy():
    code_dir = '~/oneway'
    with cd(code_dir):
        run("git pull")
