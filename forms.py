from django.forms import ModelForm
from shop.models import Product


class AddItemToCartForm(ModelForm):

    class Meta:
        model = Product
        fields = ['name', 'price']
