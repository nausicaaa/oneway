from django.conf import settings
from django.conf.urls import include, patterns, url
from django.conf.urls.static import static
from django.contrib import admin

from shop.views import (CategoryDetailView, CategoryListView, HomeTemplateView,
                        ProductDetailView, ProductListView, CartDetailView)

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', HomeTemplateView.as_view()),
    url(r'^products/?$', ProductListView.as_view()),
    url(r'^products/(?P<slug>[-\w]+)/?$', ProductDetailView.as_view()),
    url(r'^categories/?$', CategoryListView.as_view()),
    url(r'^categories/(?P<slug>[-\w]+)/?$', CategoryDetailView.as_view()),
    url(r'^cart/(?P<pk>\d+)/?$', CartDetailView.as_view()),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)